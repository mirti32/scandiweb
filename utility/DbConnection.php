<?php

class DbConnection
{
	private $servername = "localhost";
	private $username = "root";
	private $password = "";
	private $connection ;
	private static $instance = null;

	private function __construct()
	{
		$this->connection = mysqli_connect($this->servername,$this->username,$this->password);	
	}
	
	public static function getInstance()
	{
		if (self::$instance == null)
		{
			self::$instance = new DbConnection();
		}
		return self::$instance;
	}

	public function getConnection()
	{
	  return $this->connection;
	}
}
?>