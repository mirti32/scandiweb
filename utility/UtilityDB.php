<?php
require_once ("DbConnection.php");


Class UtilityDB
{   
    private $connection;
    
    function __construct()
    {
        $istance = DbConnection::getInstance();
        $this->connection = $istance->getConnection();
    }

    function isSku($sku)
    {
        $sql = "SELECT * FROM scanditest.products WHERE SKU='$sku'";
        $result = $this->connection->query($sql);
        return $result->num_rows == 0; 
    }    


    function isTypeFromId($id)
    {   
        $sql = "SELECT type FROM scanditest.products_type WHERE `type_id` = $id ";
        $result = $this->connection->query($sql);
        $row = $result->fetch_assoc();
        return $row['type'];
    }
}
?>