<?php
require_once ("../utility/UtilityDB.php");
require_once ("../model/Forniture.php");
require_once ("../model/Book.php");
require_once ("../model/Dvd.php");
require_once ("../model/Type.php");
require_once ("../model/Field.php");



class DataManager {

	public static function fromRowToProduct($row)
	{
		$class = $row['type'];
		$product = new $class();
		$product->setName($row['p_name']);
        $product->setId($row['id']);
        $product->setPrice($row['price']);
        $product->setSku($row['SKU']);
        $product->setTypeName($row['type']);
		return $product;
	}
	
	public static function fromRowToFieldArray($row)
	{
		return [
			'measure'=>$row['measure'],
			'value'=>$row['value'],
			'name'=>$row['f_name']
		];
	}
	
	public static function fromPostToProduct($post)
	{	$utility = new UtilityDB();
		$class = $utility->isTypeFromId($post['type_product']);
		$product = new $class();
        $product->setSku(self::validate($post['sku']));
        $product->setName(self::validate($post['name']));
        $product->setPrice(self::validate($post['price']));
        $product->setTypeid(self::validateInt($post['type_product']));
        $product->setField($post['field']);
		return $product;
	}

	public static function fromRowToField($row)
	{
		$fields = new Field();
		$fields->setName($row['name']);
		$fields->setMeasure($row['measure']);
		$fields->setIdfield($row['id_field']);
		return $fields;
	}

	public static function fromRowToTypeArray($row)
	{
		$types = new Type();
		$types->SetTypeId($row['type_id']);
		$types->SetType($row['type']);
		return $types;
	}

	public static function delete($type,$id)
	{	
		for($i=0;$i<count($id);$i++){
			$product = new $type[$i];
			$product->delete($id[$i]);
		}
	}

	/**
	 * functions for validation
	 */
	private static function validate($value)
	{
		if(empty($value)){
			$message=["code"=>1,"message"=>"You must to fill all fields"];
			echo json_encode($message);
			exit;
		} else {
			return $value;
		}
	}

	private static function validateInt($value)
	{
		if(empty($value)){
			$message = ["code"=>1,"message"=>"You must to fill all fields"];
			echo json_encode($message);
			exit;
		} else if ($value<0){
			$message = ["code"=>1,"message"=>"you have to select a type of product"];
			echo json_encode($message);
			exit;
		} else {
			return $value;
		}
	}
}

?>