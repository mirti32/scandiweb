<?php

require_once ("../model/ProductList.php");
require_once("../utility/UtilityDB.php");
require_once("../utility/DataManager.php");

$load= new ProductList();
$list=$load->loadProducts();
$product_list = array();
$utilityDB= new UtilityDB();
while ($row = $list->fetch_assoc()){   
    if(array_key_exists($row['id'],$product_list) ){
        $product = $product_list[$row['id']];
        $field_array = $product->getField();
    } else {
        $product = DataManager::fromRowToProduct($row);
        $field_array=array();
    }
    $field_array [] = DataManager::fromRowToFieldArray($row);
    $product->setField($field_array);
    $product_list[$row['id']] = $product;
}
echo json_encode($product_list);
  
?>