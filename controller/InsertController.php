<?php
require_once("../utility/UtilityDB.php");
require_once("../utility/DataManager.php");


if(isset($_POST['sku'])){
    $sku = $_POST['sku'];
    $check = new UtilityDB();
    $is_unique = $check->isSku($sku);
    if($is_unique){
        $product = DataManager::fromPostToProduct($_POST);
        $product->insertProduct($product);  
        $message=["code"=>0,"message"=>"Product inserted"];
    } else {
        $message=["code"=>1,"message"=>"SKU is already in system"];
    }
    echo json_encode($message);
} 

?>