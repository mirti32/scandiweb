<?php
require_once ("../model/Type.php");
require_once ("../utility/DataManager.php");
$load = new Type();
$typelist = $load->loadType();
$type_array = array();
while ($row = $typelist->fetch_assoc()){   
    $type_array[] = DataManager::fromRowToTypeArray($row);
}  
echo json_encode($type_array);

?>