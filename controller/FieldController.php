<?php

require_once ("../model/Field.php");
require_once("../utility/DataManager.php");

if($_POST){   
    $load = new Field();
    $fieldList = $load->loadField($_POST['values']);
    $field_array = array();
        while ($row = $fieldList->fetch_assoc()){
            $field_array[] = DataManager::fromRowToField($row);
        }
    echo json_encode($field_array);
}
?>