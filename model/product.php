<?php
require_once("../utility/Dbconnection.php");

abstract class Product implements JsonSerializable 
{
    private $id;
    private $sku;
    private $name;
    private $price;
    private $field;
    private $id_field;
    private $type;
    private $type_id;
    private $f_name;
    private $measure;
    private $value;
    private $connection;
    
    function __construct()
    {
        $istance = DbConnection::getInstance();
        $this->connection = $istance->getConnection();
    }
    
    function setId($a)
    {
        $this->id = $a;
    }

    function setSku($b)
    {
        $this->sku = $b;
    }

    function setName($c)
    {
        $this->name = $c;
    }

    function setPrice($d)
    {
        $this->price = $d;

    }
    function setIdfield($f)
    {
        $this->idfield = $f;
    }

    function setValue($g)
    {
        $this->value=$g;
    }

    function setTypeName($b)
    {
        $this->type= $b;
    }

    function setField($e)
    {
        $field_array = array();
        foreach($e as $value){
            $field_array[] = $value;
        }
        $this->field = $field_array;
    }
    
    function setMeasure($c)
    {
        $this->measure= $c;
    }
    function setFieldName($f)
    {
        $this->measure= $f;
    }
    function setFieldValue($g)
    {
        $this->measure= $g;
    }

    function getId()
    {
        return $this->id;
    }

    function getSku()
    {
        return $this->sku;
    }

    function getName()
    {
        return $this->name;
    }

    function getPrice()
    {
        return $this->price;
    }

    function getField()
    {
        return $this->field;
    }    

    function getTypeName()
    {
        return $this->type;
    }

    function setTypeId($j)
    {
        $this->type_id = $j;
    }

    function setType($k)
    {
        $this->type= $k;
    }

    function getTypeId()
    {
        return $this->type_id;
    }

    function getType()
    {
        return $this->type;
    }

    function delete($id)
    {   
        $sql= "DELETE FROM scanditest.products, scanditest.product_field_value
        USING scanditest.products 
        INNER JOIN scanditest.product_field_value 
        ON scanditest.products.id = scanditest.product_field_value.id_product  WHERE scanditest.products.id = $id";
        $result = $this->connection->query($sql);
        
        return $result;   
    }
        
    
    function insertProduct($product)
    {   
        //insert of product
        $sku = $product->getSku();
        $name = $product->getName();
        $price = $product->getPrice();
        $type = $product->getTypeid();
        $sql = "INSERT INTO scanditest.products (`SKU`, `name`, `price`, `id_type`) VALUES('$sku','$name','$price','$type')";
        $this->connection->query($sql);
        $get = "SELECT id from scanditest.products WHERE SKU = '$sku'";
        $result = $this->connection->query($get);
        $row = $result->fetch_assoc();
        $id_product = $row['id'];
        //insert product attributes 
        foreach($product->getField() as $f){
            $sqlc =  "INSERT INTO scanditest.product_field_value (`id_product`,`id_field`,`value`) VALUES('$id_product',".$f['field'].",".$f['value'].")";
            $this->connection->query($sqlc);
        }     
    }


    function jsonSerialize() 
    {
        return [
            'name' => $this->name,
            'price' => $this->price,
            'field' => $this->field,
            'type_product' => $this->getTypeName(),
            'sku' => $this->sku,
            'id' => $this->id,
        ];
    }
    
}

?>