<?php
require_once("../utility/Dbconnection.php");

class Type implements JsonSerializable
{
    private $type;
    private $type_id;
    private $connection;

    function __construct()
    {
        $istance = DbConnection::getInstance();
        $this->connection = $istance->getConnection();
    }

    function setTypeId($a)
    {
        $this->type_id = $a;
    }

    function setType($b)
    {
        $this->type= $b;
    }

    function getTypeId()
    {
        return $this->type_id;
    }

    function getType()
    {
        return $this->type;
    }

    function loadType()
    {   
        $sql = "SELECT * FROM scanditest.products_type";
        $result = $this->connection->query($sql);
        return $result;
    }


    function JsonSerialize()
    {
        return [
            'type_id'=> $this->type_id,
            'type' => $this->type
        ];
    }

}
?>