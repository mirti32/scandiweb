<?php
require_once("../utility/Dbconnection.php");

class ProductList
{   
    private $connection;

    function __construct()
    {
        $istance = DbConnection::getInstance();
        $this->connection = $istance->getConnection();
    }

    function loadProducts()
    {   
        $sql = "SELECT p.id,p.SKU,p.name as p_name,p.price,p.id_type,pt.type,f.name as f_name,f.measure,fv.value
                FROM scanditest.products p JOIN scanditest.products_type pt
                    ON p.id_type = pt.type_id
                        JOIN scanditest.field f 
                    ON p.id_type = f.type_id
                        JOIN  scanditest.product_field_value fv 
                    ON p.id = fv.id_product
                    where fv.id_field = f.id_field";
        $result= $this->connection->query($sql);
        return $result;
    }
}