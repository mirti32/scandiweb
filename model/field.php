<?php
require_once("../utility/Dbconnection.php");

class Field  implements JsonSerializable
{
    private $field;
    private $idfield;
    private $name;
    private $measure;
    private $value;
    private $connection;
    
    function __construct()
    {
        $istance = DbConnection::getInstance();
        $this->connection = $istance->getConnection();
    }

    function setIdfield($a)
    {
        $this->idfield = $a;
    }
 
    function setName($b)
    {
        $this->name= $b;
    }

    function setMeasure($c)
    {
        $this->measure= $c;
    }
    
    function setValue($d)
    {
        $this->value=$d;
    }

    function getIdfield()
    {
        return $this->idfield;
    }

    function getName()
    {
        return $this->name;
    }

    function getMeasure()
    {
        return $this->measure;
    }
    
    function getValue()
    {
        return $this->value;
    }

    function loadField($id)
    {   
        $sql = "SELECT * FROM scanditest.field WHERE type_id=$id";
        $result = $this->connection->query($sql);
        return $result;
    }

    function JsonSerialize()
    {
        return [
            'name' => $this->name,
            'measure' => $this->measure,
            'value' => $this->value,
            'id_field' => $this->idfield
        ];
    }
}
?>