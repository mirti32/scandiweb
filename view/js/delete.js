$( document ).ready(function() {
    $('#button').click(function(event){  
        var id = [];
        var type = [];

        $('.form-control-input:checkbox:checked').each(function(i){
            id[i] = $(this).val();
            type[i]=$('#type-'+id[i]).val();
        });
        console.log(id,type);

        $.ajax({
            type:'POST',
            url:'../controller/DeleteController.php',
            data: {id,type},
            dataType: 'json',
            success: function(response){

                $("#message").children().remove();
                if(response['code']==0){
                    $('#message').append("<div class='alert alert-success alert-dismissible fade show' role='alert'>"+response['message']+"</div>");
                } else{
                    $('#message').append("<div class='alert alert-warning alert-dismissible fade show' role='alert'>"+response['message']+"</div>");
                } 
                $.ajax(
                    {
                        type: "GET",
                        url: '../controller/ListController.php',
                        data: '',
                        dataType: 'json',
                        success: function (response)
                        {
                        $("#list").children().remove();
                        $.each(response,function()
                        {
                            
                            var id = this['id'];
                            var sku = this['sku'];
                            var name = this['name'];
                            var type = this['type_product'];
                            var measure = this['measure'];
                            var field = this['field'];
                            var price = this['price'];
                            var html = "<div class='col-3'>"+
                            "<div class='card'>"+
                            "<div class='card-header'>"+
                            " <input type='checkbox' class='form-control-input' id='selection' value="+id+"> </div>" +
                            "<input type='hidden' class='form-control-input' id= 'type-"+id+"' name = product[] value="+type+"> " +
                            " <div class='card-body text-center'> <p class='card-title'>"+name+"</p> " +  
                            " <p class='card-subtitle mb-2 text-muted'>SKU : "+sku+"</p> "+
                            " <p class='card-subtitle mb-2 text-muted'>Product type : "+type+"</p> "+
                            " <p class='card-subtitle mb-2 text-muted'>Price : "+price+" Eur</p> ";
                            $.each(this.field,function(){
                                html = html + "<p class='card-subtitle mb-2 text-muted'>" +this['name']+" : "+this['value']+ " "+this['measure']+" </p> ";
                            });
                            html = html + "</div></div></div>";
            
                            $('#list').append(html);
                        })
                        } 
                        
            
                    }
                )    
            }
        })
        
    })

    $("#selectall").change(function(){
        var checked = $(this).is(':checked'); // Checkbox state
   
        if(checked){
            $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
        }else{
          $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
        }
    
     });
});