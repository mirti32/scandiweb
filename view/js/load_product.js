

$( document ).ready(function() {
    $.ajax(
        {
            type: "GET",
            url: '../controller/ListController.php',
            data: '',
            dataType: 'json',
            success: function (response)
            {
            
            $.each(response,function()
            {
                
                var id = this['id'];
                var sku = this['sku'];
                var name = this['name'];
                var type = this['type_product'];
                var measure = this['measure'];
                var field = this['field'];
                var price = this['price'];
                var html = "<div class='col-3'>"+
                "<div class='card'>"+
                "<div class='card-header'>"+
                "<input type='checkbox' class='form-control-input' name = product[] id='selection' value="+id+"> </div>" +
                "<input type='hidden' class='form-control-input' id= 'type-"+id+"' name = product[] value="+type+"> " +
                " <div class='card-body text-center'> <p class='card-title'>"+name+"</p> " +  
                " <p class='card-subtitle mb-2 text-muted'>SKU : "+sku+"</p> "+
                " <p class='card-subtitle mb-2 text-muted'>Product type : "+type+"</p> "+
                " <p class='card-subtitle mb-2 text-muted'>Price : "+price+" Eur</p> ";
                $.each(this.field,function(){
                    html = html + "<p class='card-subtitle mb-2 text-muted'>" +this['name']+" : "+this['value']+ " "+this['measure']+" </p> ";
                });
                html = html + "</div></div></div>";

                $('#list').append(html);
            })
            } 
            

        }
    )
});