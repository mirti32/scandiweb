$( document ).ready(function() {
    //require type of product data
     $.ajax({
        type: "POST",
        url: '../controller/TypeController.php',
        data: '',
        success: function(response)
        {   
            var len = response.length;
            for( var i = 0; i<len; i++){
                var id = response[i]['type_id'];
                var name = response[i]['type'];
                
                $("#sel_type").append("<option value='"+id+"'>"+name+"</option>");

         } },
        dataType: 'json',
        });

        $('#sel_type').change(function()
        {
         
            $("#type_field").children().remove();
          
          var value = $('#sel_type option:selected').val();

       //require type of field   
          $.ajax(
            {
                type: "POST",
                url: '../controller/FieldController.php',
                data: {values:value},
                dataType: 'json',
                success: function (response)
                {
                
                $.each(response,function(){
                
                    var id = this['id_field'];
                    var name = this['name'];
                    var measure = this['measure'];
                    $('#type_field').append("<label> "+name+"</label> <input type='text' class= 'form-control product-field' name='"+id+"'>")
                })
                } 
            }
        )
    })
});