$( document ).ready(function() {
    $('#button').click(function(event) 
    {  

        var field = $("input:text.product-field");
        var array_field =[];
        $.each(field,function(){
            
            array_field.push({'field':this.name,'value':this.value});
        })
        
        var data = {
            'sku'    : $('input[name=SKU]').val(),
            'name'   : $('input[name=name]').val(),
            'price': $('input[name=price]').val(),
            'type_product' : $('#sel_type option:selected').val(),
            'field' : array_field
        };
        console.log(data);
        var values = [];
            $("input[name='field[]']").each(function() {
                values.push($(this).val());
            });

        // process data for submit
        $.ajax({
            type        : 'POST', 
            url         : '../controller/InsertController.php',
            data        :  data, 
            dataType    : 'json',
            success: function (response)
            {
                $("#message").children().remove();
                if(response['code']==1){
                    $('#message').append("<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+response['message']+"</div>");
                } else{
                    $('#message').append("<div class='alert alert-success alert-dismissible fade show' role='alert'>"+response['message']+"</div>");
                } 
            }                 
            
        })
        
        
       
    });
});