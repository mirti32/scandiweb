-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Mar 06, 2020 alle 13:54
-- Versione del server: 5.7.26
-- Versione PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scanditest`
--
CREATE DATABASE IF NOT EXISTS `scanditest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `scanditest`;

-- --------------------------------------------------------

--
-- Struttura della tabella `field`
--

DROP TABLE IF EXISTS `field`;
CREATE TABLE IF NOT EXISTS `field` (
  `id_field` int(1) NOT NULL,
  `type_id` int(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `measure` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_field`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `field`
--

INSERT INTO `field` (`id_field`, `type_id`, `name`, `measure`) VALUES
(1, 1, 'Size', 'MB'),
(2, 2, 'Weight', 'KG'),
(3, 3, 'Height', 'cm'),
(4, 3, 'Width', 'cm'),
(5, 3, 'Lenght', 'cm');

-- --------------------------------------------------------

--
-- Struttura della tabella `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `SKU` varchar(9) NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `id_type` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `products`
--

INSERT INTO `products` (`id`, `SKU`, `name`, `price`, `id_type`) VALUES
(29, 'FF-001', 'Chair', 45, 3),
(30, 'FF-002', 'Chair', 45, 3),
(31, 'FF-003', 'Chair', 45, 3),
(32, 'FF-004', 'Chair', 45, 3),
(33, 'DD-001', 'Pocahontas', 12, 1),
(34, 'DD-002', 'Pocahontas', 12, 1),
(35, 'DD-003', 'Pocahontas', 12, 1),
(36, 'DD-004', 'Pocahontas', 12, 1),
(37, 'BB-001', 'Alice in Wonderland', 12, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `products_type`
--

DROP TABLE IF EXISTS `products_type`;
CREATE TABLE IF NOT EXISTS `products_type` (
  `type_id` int(1) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `products_type`
--

INSERT INTO `products_type` (`type_id`, `type`) VALUES
(1, 'DVD-disc'),
(2, 'Book'),
(3, 'Forniture');

-- --------------------------------------------------------

--
-- Struttura della tabella `product_field_value`
--

DROP TABLE IF EXISTS `product_field_value`;
CREATE TABLE IF NOT EXISTS `product_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_field` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_product` (`id_product`),
  KEY `id_field` (`id_field`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `product_field_value`
--

INSERT INTO `product_field_value` (`id`, `id_product`, `id_field`, `value`) VALUES
(14, 29, 3, 100),
(15, 29, 4, 20),
(16, 29, 5, 50),
(17, 30, 3, 100),
(18, 30, 4, 20),
(19, 30, 5, 50),
(20, 31, 3, 100),
(21, 31, 4, 20),
(22, 31, 5, 50),
(23, 32, 3, 100),
(24, 32, 4, 20),
(25, 32, 5, 50),
(26, 33, 1, 700),
(27, 34, 1, 700),
(28, 35, 1, 700),
(29, 36, 1, 700),
(30, 37, 2, 1);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `field`
--
ALTER TABLE `field`
  ADD CONSTRAINT `field_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `products_type` (`type_id`);

--
-- Limiti per la tabella `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `products_type` (`type_id`);

--
-- Limiti per la tabella `product_field_value`
--
ALTER TABLE `product_field_value`
  ADD CONSTRAINT `product_field_value_ibfk_1` FOREIGN KEY (`id_field`) REFERENCES `field` (`id_field`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
